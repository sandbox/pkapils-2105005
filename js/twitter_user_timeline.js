(function($jq) {
Drupal.behaviors.twitterProfileWidget = function (context) {
    var speed = Drupal.settings.settings.tut_speed;
    function run(){
	var prev = $jq("#twitter_user_timeline li:first-child");
	$.unique(prev).each(function(i) {
		$(this).delay(i*600).slideUp(function() {
			$jq(this).hide().removeClass('active').appendTo(this.parentNode);
			$jq("#twitter_user_timeline li.active:last").next().addClass('active').slideDown();
		    });
	    });
    }
    window.setInterval(run,speed);
};
})(jQuery);
