<?php

function twitter_user_timeline_admin_settings_form() {
  $form['app_settings'] = array(
                           '#type' => 'fieldset',
                           '#title' => t('Twitter App OAuth settings'),
                           '#description' => t('OAuth is an authentication protocol that allows users to approve application to act on their behalf without sharing their password.You are requested to create an application <a href=@create_app>here</a> and ', array('@create_app' => 'https://dev.twitter.com/apps/new')),
                           );
  
  $form['app_settings']['tut_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer key'),
    '#default_value' => variable_get('tut_consumer_key', NULL),
    '#required' => TRUE,
  );
  $form['app_settings']['tut_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer secret'),
    '#description' => t('Keep the "Consumer secret" a secret.'),
    '#default_value' => variable_get('tut_consumer_secret', NULL),
    '#required' => TRUE,
  );
  $form['app_settings']['tut_oauth_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#default_value' => variable_get('tut_oauth_token', NULL),
    '#required' => TRUE,
  );
  $form['app_settings']['tut_oauth_token_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token secret'),
    '#default_value' => variable_get('tut_oauth_token_secret', NULL),
    '#required' => TRUE,
  );

  $form['config'] = array(
                           '#type' => 'fieldset',
                           '#title' => t('Timeline Configuration'),
                           );
  $form['config']['tut_profile_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile User'),
    '#default_value' => variable_get('tut_profile_user', NULL),
    '#required' => TRUE,
  );
  $form['config']['tut_no_tweets'] = array(
    '#type' => 'textfield',
    '#title' => t('No of Tweets'),
    '#default_value' => variable_get('tut_no_tweets', 10),
    '#description' =>t('Includes retweet in the count, even "Recent Tweets" is disabled')
  );
  $form['config']['tut_tweets_show'] = array(
    '#type' => 'textfield',
    '#title' => t('No of tweets to show'),
    '#default_value' => variable_get('tut_tweets_show', 3),
  );
  $form['config']['tut_include_retweets'] = array(
    '#type' => 'radios',
    '#title' => t('Include Retweets'),
    '#default_value' => variable_get('tut_include_retweets', 0),
    '#options' => array(t('No'), t('Yes')),
  );
  $form['config']['tut_exclude_replies'] = array(
    '#type' => 'radios',
    '#title' => t('Exclude Replies'),
    '#default_value' => variable_get('tut_exclude_replies', 1),
    '#options' => array(t('No'), t('Yes')),
  );
  $form['config']['tut_recent_tweets'] = array(
    '#type' => 'select',
    '#title' => t('Recent Tweets'),
    '#default_value' => variable_get('tut_recent_tweets', 0),
    '#options' => array_merge( array(t('All')), range(1,7) ),
    '#description' =>t('in days')
  );
  $form['config']['tut_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Speed Interval'),
    '#default_value' => variable_get('tut_speed', 5000),
    '#description' =>t('in milliseconds')
  );
  $form['config']['tut_disable_effects'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Effects'),
    '#default_value' => variable_get('tut_disable_effects', FALSE),
  );
  $form['#validate'][] = 'twitter_user_timeline_admin_validate';
  return system_settings_form($form);
  }


/**
 * Validate  admin form
 */
function twitter_user_timeline_admin_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!ctype_digit($values['tut_no_tweets']) || $values['tut_no_tweets'] > 200) {
    form_set_error('tut_no_tweets', t('Please, enter only digits from 1 to 200 in "No of Tweets" field'));
  }
  if (!ctype_digit($values['tut_tweets_show'])) {
    form_set_error('tut_tweets_show', t('Please, enter only digits in "No of tweets to show" field'));
  }
  elseif($values['tut_tweets_show'] > $values['tut_no_tweets']){
    form_set_error('tut_tweets_show', t('Please, enter "No of tweets to show" less than or equal to "No of Tweets"'));
  }
  if (!ctype_digit($values['tut_speed']) || $values['tut_speed']%1000 != 0 ) {
    form_set_error('tut_speed', t('Please, enter only digits in multiples of 1000 in "Speed Interval" field'));
  }
}