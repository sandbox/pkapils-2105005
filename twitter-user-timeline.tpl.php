<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
  <script type="text/javascript">
    var $jq = jQuery.noConflict();
  </script> 
<div id = "twitter_user_timeline">
   <ul class="feeds">
   <?php $i = 0;?>
   <?php foreach ($tweets as $tweet){ ?>
   <?php $i++;
         $hide = ($tweets_show < $i)? "style='display:none'" : NULL;   
         $active = ($tweets_show >= $i)? " active" : NULL; ?>
        <?php print "<li class='twitter-article{$active}' {$hide} >";?>
         <?php print "<a href='https://twitter.com/{$tweet->user->screen_name}/status/{$tweet->id_str}' class='tweet-time'>".relative_time($tweet->created_at)." </a>"; ?>
         <div class="header">
             <?php print "<a href='https://twitter.com/{$tweet->user->screen_name}'>" ?>
               <?php print "<img src='{$tweet->user->profile_image_url_https}' class='avatar' />"; ?>
                                      <?php print "<span class='p-name'>{$tweet->user->name}</span>"; ?>              
                                      <?php print "<span class='p-nickname'>@{$tweet->user->screen_name}</span>"; ?>
             </a>                            
         </div>
         <div class="twitter-text">
            <p>
                  <?php $text = preg_replace( '/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;>\:\s\<\>\)\]\!])/', '<a href=${1} class="link">${1}</a>',$tweet->text); 
                  $text = preg_replace( '/\B@([_a-zA-Z0-9]+)/', '<a href="https://twitter.com/${1}" class="p-nickname">@<b>${1}</b></a>',$text); 
                  print preg_replace( '/\B#([_a-zA-Z0-9]+)/', '<a href="https://twitter.com/search?q=%23${1}&src=hash" class="hash">#<b>${1}</b></a>',$text);?>
           </p>
         </div>
      </li>
   <?php } ?>
   </ul>
</div>
<?php
   function relative_time($time){
     $time = explode(' ', $time);
     $compound_format = $time[2].'/'.$time[1].'/'.$time[5].':'.$time[3].' '.$time[4];
     $diff = time() - strtotime($compound_format);
     if ($diff<60)
       return $diff . "s";
     $diff = round($diff/60);
     if ($diff<60)
       return $diff . "m";
     $diff = round($diff/60);
     if ($diff<24)
       return $diff . "h";
     $diff = round($diff/24);
     if ($diff<7)
       return date('d M', strtotime($compound_format));
     $diff = round($diff/360);
     if ($diff<12)
       return date('d M y', strtotime($compound_format));
     }
?>